﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ApiModel
{
    public class LiteralModel 
    {
        private string val;
        public string Value { get => val ?? ""; set => this.val = value; }

        private IImmutableList<LiteralModel> values;
        public IImmutableList<LiteralModel> Values
        {
            get => values ?? ImmutableList.Create<LiteralModel>();
            set => values = value;
        }

        public LiteralModel() { }

        public LiteralModel(int value)
        {
            Value = value.ToString();
        }

        public LiteralModel(string value)
        {
            Value = value ?? "";
        }

        public LiteralModel(IImmutableList<LiteralModel> values)
        {
            Value = "";
            Values = values ?? ImmutableList.Create<LiteralModel>();
        }

        /// <summary>
        /// tuple constructor
        /// </summary>
        /// <param name="value">tuple name</param>
        /// <param name="values"></param>
        public LiteralModel(string value, IImmutableList<LiteralModel> values)
        {
            Value = value;
            Values = values;
        }

    }
}
