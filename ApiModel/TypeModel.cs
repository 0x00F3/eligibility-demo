﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModel
{
    public class TypeModel
    {
        public string Name { get; set; }

        //represents a generic
        public TypeModel T { get; set; }

        public TypeModel() {  }

        public TypeModel(string name)
        {
            Name = name;
        }

        public TypeModel(string name, TypeModel t)
        {
            Name = name;
            T = t;
        }
        
    }
}
