﻿
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    public class ExpressionModel
    {
        public string Name { get; set; }

     
        public IImmutableList<ArgumentModel> Arguments { get; set; }


        private LiteralModel literal;
        public LiteralModel Literal
        {
            get => literal ?? (literal = new LiteralModel());
            set => literal = value;
        }
        
    }
}
