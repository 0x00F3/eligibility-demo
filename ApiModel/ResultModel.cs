﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace ApiModel
{
    /// <summary>
    /// a json-friendly result
    /// </summary>
    public class ResultModel
    {
        public LiteralModel Literal { get; set; }
        public IImmutableList<string> Errors { get; set; }


        /// <summary>
        /// SERIALIZER USE ONLY.
        /// </summary>
        public ResultModel() { }

        public ResultModel(LiteralModel literal)
        {
            Literal = literal;
        }

        public ResultModel(IImmutableList<string> errors)
        {
            Errors = errors;
        }
    }
}
