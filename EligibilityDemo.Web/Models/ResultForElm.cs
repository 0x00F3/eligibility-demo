﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EligibilityDemo.Web.Models
{
    /// <summary>
    /// contains the responseCode Elm is looking for. 
    /// </summary>
    public class ResultForElm
    {
        public int result { get; set; }
    }
}
