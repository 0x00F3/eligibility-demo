﻿using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using EligibilityDemo.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EligibilityDemo.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private const string CONSTANTS_URL 
            = "http://localhost:6717/Home/Constants";

        private const bool REMOTE_DEBUG = false;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Constants()
        {
            if(REMOTE_DEBUG)
            {
                return new ContentResult
                {
                    Content = "{\"minEarlyAge\":60,\"minEarlyMonths\":360,\"minLaterAge\":62,\"minLaterAgeLowerMonths\":60,\"minLaterAgeHigherMonths\":120}",
                    ContentType = "application/json"
                };
            }
            else
            {
                HttpResponseMessage responseMessage
                    = await new HttpClient().GetAsync(CONSTANTS_URL);
                string constantsJson 
                    = await responseMessage.Content.ReadAsStringAsync();
                return new ContentResult
                {
                    Content = constantsJson,
                    ContentType = "application/json"
                };
            }
            
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
