﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ApiModel;
using Core;
using EligibilityDemo.Data;
using EligibilityDemo.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace EligibilityDemo.Web.Controllers
{
    public class ApiController : Controller
    {
        private const string CONSTANTS_URL
            = "http://localhost:6717/Home/Constants";

        private const string DATA_URL = "placeholder";

        public async Task<IActionResult> Eligible(string ssn)
        {
            PersistenceModel persistence = await new PersistenceRepository()
                .Get(ssn);
            ConstantsModel constants = await new ConstantsRepository().Get();
            ExecutionModel executable = new ExecutionModel
            {
                Dfn = await new RulesRepository().Get(),
                RunArgs = toRunArgs(persistence, constants.MinLaterAgeLowerMonths)
            };
            ResultModel answer = await new RuntimeRepository().Exec(executable);
            return Json(toResultForElm(answer.Literal));
        }

        private ResultForElm toResultForElm(LiteralModel literal)
            => literal.Value.ParseInt()
                .Match(some => new ResultForElm { result = some }
                , () => new ResultForElm { result = -1 });
        

        private IImmutableList<RunArgModel> toRunArgs(PersistenceModel model, int minAgeLowerMonths)
            => ImmutableList.Create(new RunArgModel("age", model.AgeInYears().ToString())) // old enough
                    .Add(new RunArgModel("service-months", model.TotalMilitaryAndCreditMonths.ToString())) // just barely enough
                    .Add(new RunArgModel("before-1995", model.Before1995(minAgeLowerMonths).ToString())); // qualifying
        
    }
}