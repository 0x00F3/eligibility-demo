module Main exposing (..)

import Browser
import Html exposing ( .. )
import Html.Attributes exposing ( .. )
import Html.Events exposing ( .. )
import Http
import Json.Decode exposing (Decoder, field, int)
import Json.Decode.Pipeline exposing ( required )


-- MAIN


main =
  Browser.element 
  { init = init
  , update = update
  , subscriptions = subscriptions
  , view = view }



-- MODEL

type alias Constants = 
  { minEarlyAge : Int
  , minEarlyMonths : Int
  , minLaterAge : Int
  , minLaterAgeLowerMonths : Int
  , minLaterAgeHigherMonths : Int
  }



type alias Model = 
  { responseCode : Int
  , ssn : String
  , maybeConstants : Maybe Constants
  }

init : () -> (Model, Cmd Msg)
init _ =
  ({
   responseCode = -1
  , ssn = ""
  , maybeConstants = Nothing
  }
  , Http.get { url = "/Home/Constants", expect = Http.expectJson GotConstants constantsDecoder}
  )



-- UPDATE

type Msg 
  = GotConstants (Result Http.Error Constants)
  | GotEligibility (Result Http.Error Int)
  | SetSSN String

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    GotConstants result -> 
      case result of
        Ok response -> ({model | maybeConstants = Just response}, Cmd.none)
        Err _ -> (model, Cmd.none)
    GotEligibility result -> 
      case result of 
        Ok response -> 
          ({model | responseCode = response}, Cmd.none)
        Err _ -> (model, Cmd.none)
    SetSSN ssn -> 
      if String.length ssn == 9 then
          ({ model | ssn = ssn}, getAnswer ssn)
      else ({ model | ssn = ssn}, Cmd.none)

-- VIEW

view : Model -> Html Msg
view model =
  div [id "elm-root"]
    [ h2 [ class "page-header" ] [text "Eligibility Calculator"]
    , label [for "ssn"] [text "Please enter a 9-digit SSN: "]
    , input [ class "input-ssn"
            , id "ssn"
            , onInput SetSSN
            , placeholder "123456789"
            , type_ "text"
            , value model.ssn
            ]
            []
    , case model.maybeConstants of
        Just consts -> flowchart consts model.responseCode
        Nothing -> text ""
    ]

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

-- helper

constantsDecoder: Decoder Constants
constantsDecoder = 
  Json.Decode.succeed Constants
    |> Json.Decode.Pipeline.required "minEarlyAge" int
    |> Json.Decode.Pipeline.required "minEarlyMonths" int
    |> Json.Decode.Pipeline.required "minLaterAge" int
    |> Json.Decode.Pipeline.required "minLaterAgeLowerMonths" int
    |> Json.Decode.Pipeline.required "minLaterAgeHigherMonths" int


flowchart : Constants -> Int -> Html Msg
flowchart model responseCode = 
  div [ class "flowchart" ]
      [ p [ class ("question" ++ path (responseCode > 0))] [ text ("Are you " ++ String.fromInt model.minEarlyAge ++ " years old or older?") ]
      , div [ class "answers"]
        [ div [class "centered flex-column flex-1"] 
          [ div [class ("down-arrow" ++ path(responseCode > 50000))] []
            , div [class "flex-1"] [span [ class ("answer yes" ++ path(responseCode > 50000))] [text "Yes"]]
            , div [class ("down-arrow"++ path(responseCode > 50000))] []
            , p [ class ("question"++ path(responseCode > 50000))] [ text ("Are you " ++ String.fromInt model.minLaterAge ++ " years old or older? ")]
            , div [ class "answers"]
              [ div [class "centered flex-column flex-1"] 
                [ div [class ("down-arrow"++ path(responseCode > 55000))] []
                  , div [class "flex-1"] [span [ class ("answer yes"++ path(responseCode > 55000))] [text "Yes"]]
                  , div [class ("down-arrow"++ path(responseCode > 55000))] []
                  , p [ class ("question"++ path(responseCode > 55000))] [ text ("Do you have at least " ++ String.fromInt model.minLaterAgeLowerMonths ++ " months of service?")]
                  , div [ class "answers"]
                    [ div [class "centered flex-column flex-1"] 
                      [ div [class ("down-arrow"++ path(responseCode > 55500))] []
                        , div [class "flex-1"] [span [ class ("answer yes"++ path(responseCode > 55500))] [text "Yes"]]
                        , div [class ("down-arrow"++ path(responseCode > 55500))] []
                        , p [ class ("question"++ path(responseCode > 55500))] [ text ("Do you have at least " ++ String.fromInt model.minLaterAgeHigherMonths ++ " months of service?")]
                        , div [ class "answers"]
                          [ div [class "centered flex-column"] 
                            [ div [class ("down-arrow"++ path(responseCode > 55550))] []
                              , div [class ""] [span [ class ("answer yes"++ path(responseCode > 55550))] [text "Yes"]]
                              , div [class ("down-arrow"++ path(responseCode > 55550))] []
                              , p [ class ("conclusion"++ path(responseCode > 55550))] [ text "Eligible."]
                            ]
                            , div [class "centered flex-column flex-1 "] 
                              [ div [class ("down-arrow"++ path(responseCode > 55500 && responseCode <= 55550 ))] []
                                , div [class ""] [span [ class ("answer no" ++ path(responseCode > 55500 && responseCode <= 55550))] [text "No"]]
                                , div [class ("down-arrow" ++ path(responseCode > 55500 && responseCode <= 55550))] []
                                , p [ class ("question" ++ path(responseCode > 55500 && responseCode <= 55550))] [ text "Are all of your service months after 1995?"]
                                , div [ class "answers"]
                                [ div [class "centered flex-column flex-1"] 
                                  [ div [class ("down-arrow" ++ path(responseCode > 55525 && responseCode <= 55550))] []
                                    , div [class ""] [span [ class ("answer yes" ++ path(responseCode > 55525 && responseCode <= 55550))] [text "Yes"]]
                                    , div [class ("down-arrow" ++ path(responseCode > 55525 && responseCode <= 55550))] []
                                    , p [ class ("conclusion" ++ path(responseCode > 55525 && responseCode <= 55550))] [ text "Eligible."]
                                  ]
                                  , div [class "centered flex-column flex-1"] 
                                    [ div [class ("down-arrow" ++ path (responseCode > 55500 && responseCode <= 55525))] []
                                      , div [class ""] [span [ class ("answer no" ++ path (responseCode > 55500 && responseCode <= 55525))] [text "No"]]
                                      , div [class ("down-arrow" ++ path (responseCode > 55500 && responseCode <= 55525))] []
                                      , p [ class ("conclusion" ++ path (responseCode > 55500 && responseCode <= 55525))] [ text "Not eligible."]
                                    ]
                                ]
                              ]
                          ]
                      ]
                      , div [class "centered flex-column"] 
                        [ div [class ("down-arrow"++path(responseCode > 55000 && responseCode < 55500))] []
                          , div [class ""] [span [ class ("answer no"++path(responseCode > 55000 && responseCode < 55500))] [text "No"]]
                          , div [class ("down-arrow"++path(responseCode > 55000 && responseCode < 55500))] []
                          , p [ class ("conclusion"++path(responseCode > 55000 && responseCode < 55500))] [ text "Not eligible."]
                        ]
                    ]
                ]
                , div [class "centered flex-column"] 
                  [ div [class ("down-arrow" ++ path (responseCode > 50000 && responseCode <= 55000))] []
                    , div [class ""] [span [ class ("answer no" ++ path (responseCode > 50000 && responseCode <= 55000))] [text "No"]]
                    , div [class ("down-arrow" ++ path (responseCode > 50000 && responseCode <= 55000))] []
                    , p [ class ("question" ++ path (responseCode > 50000 && responseCode <= 55000))] [ text ("Do you have at least " ++ String.fromInt model.minEarlyMonths ++ " months of service?") ]
                    , div [ class "answers"]
                      [ div [class "centered flex-column flex-1"] 
                        [ div [class ("down-arrow"++path(responseCode > 52500 && responseCode <= 55000))] []
                          , div [class ""] [span [ class ("answer yes"++path(responseCode > 52500 && responseCode <= 55000))] [text "Yes"]]
                          , div [class ("down-arrow"++path(responseCode > 52500 && responseCode <= 55000))] []
                          , p [ class ("conclusion"++path(responseCode > 52500 && responseCode <= 55000))] [ text "Eligible."]
                        ]
                        , div [class "centered flex-column flex-1"] 
                          [ div [class ("down-arrow"++path(responseCode > 5000 && responseCode <=52500))] []
                            , div [class ""] [span [ class ("answer no"++path(responseCode > 5000 && responseCode <=52500))] [text "No"]]
                            , div [class ("down-arrow"++path(responseCode > 5000 && responseCode <=52500))] []
                            , p [ class ("conclusion"++path(responseCode > 5000 && responseCode <=52500))] [ text "Not eligible."]
                          ]
                      ]
                  ]
              ]
          ]
          , div [class "centered flex-column"] 
            [ div [class ("down-arrow"++ path(responseCode > 0 && responseCode <= 50000))] []
              , div [class ""] [span [ class ("answer no" ++ path(responseCode > 0 && responseCode <= 50000))] [text "No"]]
              , div [class ("down-arrow"++ path(responseCode > 0 && responseCode <= 50000))] []
              , p [ class ("conclusion"++ path(responseCode > 0 && responseCode <= 50000))] [ text "Not eligibile."]
            ]
        ]
      ]
getAnswer : String -> Cmd Msg
getAnswer ssn = 
  Http.get
  { expect = Http.expectJson GotEligibility resultDecoder
  , url = "/Api/Eligible?ssn=" ++ ssn
  }

path : Bool -> String
path condition = 
  if condition then " path " else ""

resultDecoder : Decoder Int
resultDecoder = field "result" int