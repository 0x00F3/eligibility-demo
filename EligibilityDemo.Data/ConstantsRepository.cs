﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace EligibilityDemo.Data
{
    public class ConstantsRepository
    {
        private const string CONSTANTS_URL
            = "http://localhost:6717/Home/Constants";
        public async Task<ConstantsModel> Get()
        {
            HttpResponseMessage responseMessage
                    = await new HttpClient().GetAsync(CONSTANTS_URL);
            string constantsJson
                = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ConstantsModel>(constantsJson);
        }
    }
}
