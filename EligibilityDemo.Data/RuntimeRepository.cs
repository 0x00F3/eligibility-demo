﻿using ApiModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EligibilityDemo.Data
{
    /// <summary>
    /// okay, so I'm stretching the definition of "repository" a little.
    /// This is stateless, but it interfaces with an outside system so
    /// ¯\_(ツ)_/¯
    /// </summary>
    public class RuntimeRepository
    {
        private const string RUNTIME_URL = "http://localhost:60425";
        public async Task<ResultModel> Exec(ExecutionModel executable)
        {
            HttpResponseMessage responseMessage 
                = await new HttpClient().PostAsJsonAsync(RUNTIME_URL, executable);
            string answerJson = await responseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ResultModel>(answerJson);
        }
    }
}
