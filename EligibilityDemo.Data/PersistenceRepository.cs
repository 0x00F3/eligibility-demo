﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EligibilityDemo.Data
{
    public class PersistenceRepository
    {
        private const string DATA_URL = "http://localhost:1798/api/RailroaderData";
        public async Task<PersistenceModel> Get(string ssn)
        {
            HttpResponseMessage dataMessage
                = await new HttpClient().GetAsync(DATA_URL + "?id=" + ssn);
            string dataJson
                = await dataMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<PersistenceModel>(dataJson);
        }
    }
}
