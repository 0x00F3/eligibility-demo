﻿using ApiModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EligibilityDemo.Data
{
    public class RulesRepository
    {
        private const string RULES_URL = "http://localhost:6717/";
        public async Task<DefinitionModel> Get()
        {
            HttpResponseMessage rulesMessage
                = await new HttpClient().GetAsync(RULES_URL);
            string rulesJson
                = await rulesMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<DefinitionModel>(rulesJson);
        }
    }
}
