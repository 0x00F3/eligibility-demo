﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EligibilityDemo.Data
{
    public class PersistenceModel
    {
        public string Eligible { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string DOB { get; set; }
        public int AgeTotalMonths { get; set; }
        public string TotalCreditMonths { get; set; }
        public string TotalMilitaryMonths { get; set; }
        public int TotalMilitaryAndCreditMonths { get; set; }
        public string ServiceMonthsAfter1995 { get; set; }
        public string MilitaryMonthsAfter1995 { get; set; }
        public string TotalMilitaryAndServiceAfter1995 { get; set; }
        public string TotalMilitaryAndServiceBefore1995 { get; set; }
        public string Msg { get; set; }

        public int AgeInYears()
        {
            return AgeTotalMonths / 12;
        }

        // HACK HACK HAAAAACK
        public bool Before1995(int minLaterAgeLowerMonths)
        {
            return TotalMilitaryAndCreditMonths >= minLaterAgeLowerMonths;
        }
    }
}
