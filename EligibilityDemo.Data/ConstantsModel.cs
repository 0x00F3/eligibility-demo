namespace EligibilityDemo.Data
{
    public class ConstantsModel 
    {
        public int MinEarlyAge { get; set; }
        public int MinEarlyMonths {get; set; }
        public int MinLaterAge { get; set; }
        public int MinLaterAgeLowerMonths { get; set; }
        public int MinLaterAgeHigherMonths { get; set; }
    }
}