﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class Either<T1, T2>
    {
        private readonly T1 member1;
        private readonly T2 member2;
        private readonly bool useMember1;

        public Either(T1 member)
        {
            member1 = member;
            useMember1 = true;
        }

        public Either(T2 member)
        {
            member2 = member;
            useMember1 = false;
        }

        public TNext Match<TNext>(Func<T1, TNext> t1, Func<T2, TNext> t2)
            => useMember1 ? t1(member1) : t2(member2);
    }
}
