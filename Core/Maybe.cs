﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class Maybe<T>
    {
        private bool isFull;
        private readonly T value;

        public Maybe()
        {
            isFull = false;
        }

        public Maybe(T value)
        {
            this.value = value;
            isFull = this.value != null;
        }

        /// <summary>
        /// executes some on the Maybe if there is a value on which to execute,
        /// else returns an empty Maybe.
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <returns></returns>
        public Maybe<TNext> Map<TNext>(Func<T, TNext> some)
            => isFull
            ? new Maybe<TNext>(some(value))
            : new Maybe<TNext>();

        /// <summary>
        /// bind with a function that returns a Maybe to avoid nested Maybes.
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <returns></returns>
        public Maybe<TNext> FlatMap<TNext>(Func<T, Maybe<TNext>> some)
            => Match(val => some(val), () => new Maybe<TNext>());

        /// <summary>
        /// takes two functions to handle both cases of Maybe and returns the
        /// result of the appropriate one. 
        /// </summary>
        /// <typeparam name="TNext"></typeparam>
        /// <param name="some"></param>
        /// <param name="none"></param>
        /// <returns></returns>
        public TNext Match<TNext>(Func<T, TNext> some, Func<TNext> none)
            => isFull ? some(value) : none();

        public Maybe<T> RecoverWith(Maybe<T> alt)
            => Match(some => this, () => alt);
    }
}
